class Asteroid:

    def __init__(self, image, speed, posx, posy):
        self.image = image
        self.speed = speed
        self.posx = posx
        self.posy = posy
        self.pos = self.image.get_rect().move(posx, posy)

    def asteroid_collision(self, enemy_array, player_array):
        for enemy in enemy_array:
            if self.pos.colliderect(enemy.pos):
                enemy_array.remove(enemy)
        for player in player_array:
            if self.pos.colliderect(player.pos):
                player.health = 0

    def move(self, asteroid_array):
        self.pos = self.pos.move(-self.speed, 0)
        self.posx += -self.speed
        self.posy += 0
        if self.posx <= 0:
            asteroid_array.remove(self)

    def display(self, screen):
        screen.blit(self.image, self.pos)

    def update(self, screen, enemy_array, player_array, asteroid_array):
        self.asteroid_collision(enemy_array, player_array)
        self.move(asteroid_array)
        self.display(screen)
