# Here we initialize our Spaceship class(notice we don't import a single thing so there is no Pygame known in this file)
import pygame


class Spaceship:
    def __init__(self, image, speed, height, posx, posy, fire_rate):
        self.speed = speed
        self.image = image
        self.fire_rate = fire_rate
        self.spaceship_rect = image.get_rect()
        self.height = height
        self.posx = posx
        self.posy = posy
        self.pos = self.spaceship_rect.move(posx, posy)  # Starting position
        self.health = 3
        self.alive = True

    def move(self):  # self is needed as a reference to class object
        key_state = pygame.key.get_pressed()  # creating an array with bool values of pressed keys (pressed = true)
        if key_state[pygame.K_w]:
            self.pos.y -= self.speed
            self.posy -= self.speed
        if key_state[pygame.K_s]:
            self.pos.y += self.speed
            self.posy += self.speed
        if key_state[pygame.K_d]:
            self.pos.x += self.speed
            self.posx += self.speed
        if key_state[pygame.K_a]:
            self.pos.x -= self.speed
            self.posx -= self.speed
        # Protection against rolling off the screen
        if self.pos.right > 800:
            self.pos.right = self.pos.right - self.speed
            self.posx = 800
        if self.pos.left < 0:
            self.pos.left = self.pos.left + self.speed
            self.posx = 0
        if self.pos.top < 0:
            self.pos.top = self.pos.top + self.speed
            self.posy = 0
        if self.pos.bottom > 600:
            self.pos.bottom = self.pos.bottom - self.speed
            self.posy = 600 - self.height

    def get_coord(self):
        coord = [self.posx, self.posy]
        print("coordinates of spaceship: " + coord[0] + coord[1])
        return coord

    def health_update(self, player_array):
        if self.health <= 0:
            player_array.remove(self)

    def display(self, screen):
        screen.blit(self.image, self.pos)

    def update(self, screen, player_array):
        self.health_update(player_array)
        self.move()
        self.display(screen)
