# Here we initialize our Bullet class(notice we don't import a single thing so there is no Pygame known in this file)
import random


class Laser:
    def __init__(self, image, speed, posx, posy):
        self.speed = speed
        self.side_speed = 0
        self.image = image
        self.pos = image.get_rect(x=posx, y=posy)
        self.deleted = False
        self.deflected = False
        self.damage = 1
        self.enemy_colliding = False
        self.player_colliding = False
        self.colliding = False
        print("bullet fired")

    def move(self):
        self.pos = self.pos.move(self.side_speed, -self.speed)

    def deflection(self):
        random.seed()
        if not self.deflected:
            if self.colliding:
                x = random.randint(1, 2)
                if x == 1:
                    self.side_speed = self.speed
                else:
                    self.side_speed = -self.speed
                self.speed = -self.speed
                self.deflected = True

    def display(self, screen):
        screen.blit(self.image, self.pos)

    def check_collision(self, enemy_array, player_array):
        for enemy in enemy_array:
            if self.pos.colliderect(enemy.pos):
                enemy.health -= 1
                self.pos.move(0, 80)
                if not self.enemy_colliding:
                    self.enemy_colliding = True
            else:
                self.enemy_colliding = False
        for player in player_array:
            if self.pos.colliderect(player.pos):
                player.health -= 1
                self.player_colliding = True
                print(player.health)
                self.pos.move(0, -80)
            # if self in self.bullets_array:
            #    self.bullets_array.remove(self)
            else:
                self.player_colliding = False
            if self.player_colliding or self.enemy_colliding:
                self.colliding = True
            else:
                self.colliding = False

    def update(self, screen, bullets_array, enemy_array, player_array):
        self.check_collision(enemy_array, player_array)
        self.deflection()
        self.display(screen)
        self.move()
        if self.pos.bottom <= 0:
            bullets_array.remove(self)
            print("bullet removed")
        elif self.pos.top >= screen.get_height():
            bullets_array.remove(self)
            print("bullet removed")
