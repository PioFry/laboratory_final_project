# Here we initialize enemy class (notice we don't import a single thing so there is no Pygame known in this file)


class EnemySpaceship:
    def __init__(self, image, speed, posx, posy):
        self.speed = speed
        self.image = image
        self.pos = image.get_rect(x=posx, y=posy)
        self.enemyspaceship_rect = image.get_rect()
        self.posx = posx
        self.posy = posy
        self.health = 2

    def display(self, screen):
        screen.blit(self.image, self.pos)

    def move(self, x, y):
        self.pos = self.pos.move(x, y)
        self.posx += x
        self.posy += y

    def reflect(self, width):
        if self.posx == 0:
            self.speed = - self.speed
        if self.posx + 100 == width and - self.speed > 0:
            self.speed = - self.speed

    def health_update(self, enemy_array):
        if self.health <= 0:
            enemy_array.remove(self)
            print("enemy removed")

    def update(self, screen, width, enemy_array):
        self.health_update(enemy_array)
        self.move(- self.speed, 0)
        self.display(screen)
        self.reflect(width)

