# Here we initialize our Spaceship class(notice we don't import a single thing so there is no Pygame known in this file)


class Spaceship:
    def __init__(self, image, speed, posx, posy):
        self.speed = speed
        self.image = image
        self.spaceship_rect = image.get_rect()
        self.posx = posx
        self.posy = posy
        self.pos = self.spaceship_rect.move(posx, posy)  # Starting position
# Here under we define functions used by our class

    def move(self, x, y):       # self is needed as a reference to class object
        self.pos = self.pos.move(x, y)
        self.posx += x
        self.posy += y
# Protection against rolling off the screen
        if self.pos.right > 800:
            self.pos.right = self.pos.right - self.speed
        if self.pos.left < 0:
            self.pos.left = self.pos.left + self.speed
        if self.pos.top < 0:
            self.pos.top = self.pos.top + self.speed
        if self.pos.bottom > 600:
            self.pos.bottom = self.pos.bottom - self.speed

    def get_coord(self):
        coord = [self.posx, self.posy]
        print("coordinates of spaceship: " + coord[0] + coord[1])
        return coord






