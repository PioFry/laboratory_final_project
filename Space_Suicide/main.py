import pygame
import sys
import Player
import Bullet
import Enemy
import Writter
import Obstacle
import Options
import random

pygame.init()

width = 800
height = 600
screen = pygame.display.set_mode((width, height))  # first coord is width second is height
alien_image = pygame.image.load('D:\PyCharm\PyCharm_projects\Space_Suicide\Graphics\enemy.png').convert()
player_image = pygame.image.load('D:\PyCharm\PyCharm_projects\Space_Suicide\Graphics\spaceship.png').convert()
laser_image = pygame.image.load('D:\PyCharm\PyCharm_projects\Space_Suicide\Graphics\laser_ray.png').convert()
background = pygame.image.load('D:\PyCharm\PyCharm_projects\Space_Suicide\Graphics\space.jpg').convert()
asteroid_image = pygame.image.load('D:\PyCharm\PyCharm_projects\Space_Suicide\Graphics\steroid.png').convert()
# Filling screen with background
screen.blit(background, (0, 0))  # second argument is a tuple of coordinates of top left corner of the image
# Object arrays
lasers = []
aliens = []
players = []
asteroids = []
records = ["1. Piotr", "2. Wojtek", "3. Mateusz", "4. Krzysiek", "5. Stefan"]
# Initialization of objects
menu = Options.Menu(screen)
anastasia = Writter.Secretary(screen)


def create_asteroid():
    random.seed()
    y = random.randint(1, 5)
    y = y*100
    print("asteroid created")
    asteroid = Obstacle.Asteroid(asteroid_image, 20, width, y)
    asteroids.append(asteroid)


def create_player():
    player = Player.Spaceship(player_image, 10, 136, 229, 348, 15)
    players.append(player)
    print("player created")
    return player


def create_alien(speed, posx, posy):
    alien = Enemy.EnemySpaceship(alien_image, speed, posx, posy)
    aliens.append(alien)
    return alien


def create_alien_wave():
    for i in range(3):
        create_alien(10, 1000 + (i*100), 0)
        print("enemy created")


def create_laser(source, speed, x_shift, y_shift):
    laser = Bullet.Laser(laser_image, speed, source.posx + x_shift, source.posy + y_shift)
    lasers.append(laser)


def restart_game():
    lasers.clear()
    aliens.clear()
    players.clear()
    asteroids.clear()
    anastasia.fill_background()
    create_player()


def game_loop():
    # Variables starting values1
    firing_delay = 0
    wave_counter = 0
    asteroid_counter = 0

    menu.menu_update(anastasia, records)
    create_player()
    while 1:
        key_state = pygame.key.get_pressed()
        for event in pygame.event.get():  # checking for events
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    menu.menu_update(anastasia, records)
            # Creating lasers
        if key_state[pygame.K_i] and firing_delay >= players[0].fire_rate:
            create_laser(players[0], 20, 40, -110)
            firing_delay = 0

        screen.blit(background, (0, 0))  # fill screen so only 1 player image is seen at a time

        if asteroid_counter == 60:
            create_asteroid()
            asteroid_counter = 0

        if not aliens:
            create_alien_wave()
            wave_counter += 1
        if not players:
            anastasia.fill_background()
            anastasia.write("You Died, Game Over", 60, width / 2, height / 2)
            pygame.time.delay(1000)
            menu.menu_update(anastasia, records)
            restart_game()
            firing_delay = 0
            wave_counter = 0
            asteroid_counter = 0

        for player in players:
            player.update(screen, players)
        for asteroid in asteroids:
            asteroid.update(screen, aliens, players, asteroids)
        for laser in lasers:
            laser.update(screen, lasers, aliens, players)
        for alien in aliens:
            alien.update(screen, width, aliens)
            if alien.pos.left == 600 or alien.pos.left == 400 or alien.pos.left == 200:
                create_laser(alien, -20, 40, 110)

        firing_delay += 1
        asteroid_counter += 1
        print(asteroid_counter)
        if wave_counter == 4:
            anastasia.fill_background()
            anastasia.write("congratulations, you won", 60, width / 2, height / 2)
            pygame.time.delay(1000)
            sys.exit(0)

        pygame.display.update()  # make all drawn changes visible
        pygame.time.delay(100)  # without it our player would move million frames per second (manages fps)


game_loop()
