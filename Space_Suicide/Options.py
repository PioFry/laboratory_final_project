import pygame
import sys


class Menu:
    def __init__(self, screen):
        self.screen = screen
        self.in_menu = True
        self.in_records = False
        self.activeOption = 0
        self.isGameRunning = False
        self.isTutorialRunning = False

    def check_input(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                sys.exit(0)
            if event.type == pygame.KEYDOWN:
                if self.in_menu:
                    if event.key == pygame.K_1:
                        self.in_menu = False
                        self.in_records = False
                        break
                    if event.key == pygame.K_2:
                        self.in_menu = False
                        self.in_records = True
                        break
                    if event.key == pygame.K_3:
                        sys.exit(0)
                if self.in_records:
                    if event.key == pygame.K_0:
                        self.in_records = False
                        self.in_menu = True

    def draw_menu(self, writter):
        writter.write("1. New Game", 20, 400, 200)
        writter.write("2. Records", 20, 400, 350)
        writter.write("3. Quit", 20, 400, 500)

    def draw_records(self, writter, record_array):
        next_line = 0
        for name in record_array:
            writter.write(name, 10, 400, 200 + next_line)
            next_line += 70
        writter.write("Press 0 to return", 10, 700, 500)

    def menu_update(self, writter, record_array):
        self.in_menu = True
        while self.in_menu or self.in_records:
            writter.fill_background()
            if self.in_menu:
                self.draw_menu(writter)
            while self.in_menu:
                self.check_input()
            writter.fill_background()
            if self.in_records:
                self.draw_records(writter, record_array)
            while self.in_records:
                self.check_input()
