import pygame


class Secretary:
    def __init__(self, screen):
        self.screen = screen
        self.option_number = 0
        self.white = (255, 255, 255)
        self.black = (0, 0, 0)

    def get_text_surface(self, text, font):
        text_surface = font.render(text, True, self.white)
        text_rect = text_surface.get_rect()
        return text_surface, text_rect

    def write(self, text, font_size, x, y):
        large_text = pygame.font.Font('freesansbold.ttf', font_size)
        text_surface, text_rect = self.get_text_surface(text, large_text)
        text_rect.center = (x, y)
        self.screen.blit(text_surface, text_rect)
        pygame.display.update(text_rect)

    def fill_background(self):
        pygame.display.update(self.screen.fill(self.black))
